from socket import socket, AF_INET, SOCK_STREAM
from select import select
from codecs import encode, decode
from datetime import datetime
from pickle import dump, load, dumps, loads
from aes_python3 import *
from time import sleep
from hashlib import sha512
from sqlite3 import connect

#################################################

class Client(object):
    def __init__(self, socket, id, password, language, username):
        self.MOO = AESModeOfOperation()
        self.Connected = True
        
        self.Socket = socket
        self.ID = id
        self.Password = password
        self.Language = language
        self.Username = username
        
        self.Messages = []
    
    def name(self):
        return self.Username
        
    def sock(self):
        return self.Socket 
    
    def language(self):
        return self.Language 
    
    def sendto(self, message):
        if not self.isconnected():
            return -1
        try:
            self.Socket.send(dumps(AESencrypt(message, self.Password, self.MOO)))
            return 0
        except:
            self.disconnect()
            return -1
    
    def sendMessage(self, sender, message):
        print("sendMessage: Begin")
        text = "<{0}><{1}>".format(sender, message)
        messageLength = str(len(text))
        
        print("sendMessage: Text: ", text)
        print("sendMessage: Length: ", messageLength)
        
        cc = "1"
        if len(sender) > 30:
            cc = "2"
            
        print("sendMessage: NumOfRec: ", cc)
        
        if not self.isconnected():
            print("sendMessage: Isn't connected")
            return
        if self.sendto(cc) == 0:
            if self.recvOK():
                print("sendMessage: cc recieved")
                if self.sendto(messageLength) == 0:
                    if self.recvOK():
                        print("sendMessage: Length recieved")
                        if self.sendto(text) == 0:
                            if self.recvOK():
                                print("sendMessage: text recieved")
                                print("sendMessage: End")
                                return 0
        print("sendMessage: Error or disconnected somewhere")
        self.disconnect()
        
    def recvfrom(self, length):
        if not self.isconnected():
            return -1
        try:
            message = self.Socket.recv(length)
            if message == b'':
                self.disconnect
                return 1
            return AESdecrypt(loads(message),self.Password, self.MOO)
            
        except ConnectionResetError:
            self.disconnect()
            return -1
    
    def recvOK(self):
        if not self.isconnected():
            return -1
        try:
            if self.Socket.recv(64) == b'1':
                return True
            return False
        except ConnectionResetError:
            self.disconnect()
            return -1
    
    def sendOK(self):
        if not self.isconnected():
            return -1
        try:
            self.Socket.send(b'1')
            return 0
        except ConnectionAbortedError:
            self.disconnect()
            return -1
    
    def __sendMessages(self):
        if not self.Messages:
            return 0
        for message in self.Messages:
            self.sendMessage(message[0], message[1])
        self.Messages = []
        return 0
    
    def connect(self, socket):
        self.Connected = True
        self.Socket = socket
        
        firstList = getSavedMessages2(self.name().replace("0",""))
        print(firstList)
        for couple in firstList:
            sender = couple[0]
            recipient = couple[1]
            print("(Sender, Recipient): ", (sender, recipient))
            time = couple[2]
            text = couple[3]
            message = "{0}><{1}".format(time, text)
            if sender.rjust(30, "0") == self.name():
                self.Messages.append(("{0} to {1}".format("You".rjust(62, "0"), recipient.rjust(30, "0")),
                                      message))
            else:
                self.Messages.append(("{0} to {1}".format(sender.rjust(62, "0"), "You".rjust(30, "0")),
                                      message))
            
        secondList = getSavedMessages3(self.name().replace("0",""))
        for couple in secondList:
            sender = couple[0]
            recipients = couple[1].split(",")
            if not recipients[0].rjust(30, "0") == self.name() and not sender.rjust(30, "0") == self.name():
                recipient1 = recipients[0]
                recipients[1] = recipients[0]
                recipients[0] = recipient1
                del recipient1
            time = couple[2]
            text = couple[3]
            message = "{0}><{1}".format(time, text)
            if sender.rjust(30, "0") == self.name():
                self.Messages.append(("{0} to {1}, {2}".format("You".rjust(30, "0"), recipients[0].rjust(30, "0"), 
                                                                recipients[1].rjust(30, "0")),
                                      message))
            else:
                self.Messages.append(("{0} to {1}, {2}".format(sender.rjust(30, "0"), "You".rjust(30, "0"), 
                                                                recipients[1].rjust(30, "0")),
                                      message))
            
        print(self.Messages)
        
        sleep(0.07)
        return self.__sendMessages()
    
    def disconnect(self):
        self.Connected = False
        try:
            self.Socket.close()
            self.Socket = None
        except AttributeError:
            pass
        return 0
    
    def isconnected(self):
        return self.Connected
    
    def gotMessages(self, sender, message):
        self.Messages.append((sender, message))
        return 0    
    
#################################################

def saveMessage2(sender, recipient, time, message):
    connection = connect("Projektdaten.db")
    mycursor = connection.cursor()
    mycursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = [table[0] for table in mycursor.fetchall()]
    DBName = formDBName([sender, recipient], 2)
    if not DBName in tables:
        mycursor.execute("CREATE TABLE {0} (sender TEXT, recipient TEXT, time TEXT, message TEXT)".format(DBName))
        connection.commit()
    mycursor.execute("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}')".format(DBName, sender, recipient, time, message))
    connection.commit()
    return

def saveMessage3(sender, recipient1, recipient2, time, message):
    connection = connect("Projektdaten.db")
    mycursor = connection.cursor()
    mycursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = [table[0] for table in mycursor.fetchall()]
    DBName = formDBName([sender, recipient1, recipient2], 3)
    if not DBName in tables:
        mycursor.execute("CREATE TABLE {0} (sender TEXT, recipient TEXT, time TEXT, message TEXT)".format(DBName))
        connection.commit()
    mycursor.execute("INSERT INTO {0} VALUES ('{1}', '{2}', '{3}', '{4}')".format(DBName, sender,
                                                                                  "{0},{1}".format(recipient1, recipient2),
                                                                                  time, message))
    connection.commit()
    return

def getSavedMessages2(name):
    connection = connect("Projektdaten.db")
    mycursor = connection.cursor()
    mycursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = [table[0] for table in mycursor.fetchall()]
    mytables = []
    print("getSavedMessages2: tables: ", tables)
    for table in tables:
        print(table)
        if name in table and table.count("_") == 1:
            mytables.append(table)
    if mytables == []:
        return []
    number = int(20/len(mytables))
    myMessages = []
    for table in mytables:
        mycursor.execute("SELECT * FROM {0}".format(table))
        myMessages += mycursor.fetchall()[-number:]
    print(len(myMessages))
    return myMessages

def getSavedMessages3(name):
    connection = connect("Projektdaten.db")
    mycursor = connection.cursor()
    mycursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
    tables = [table[0] for table in mycursor.fetchall()]
    mytables = []
    for table in tables:
        if name in table and table.count("_") == 2:
            mytables.append(table)
    if mytables == []:
        return []
    number = int(20/len(mytables))
    myMessages = []
    for table in mytables:
        mycursor.execute("SELECT * FROM {0}".format(table))
        myMessages += mycursor.fetchall()[-number:]
    print(len(myMessages))
    return myMessages
    
def formDBName(namelist, num):
    namelist.sort()
    if num == 2:
        return "{0}_{1}".format(namelist[0], namelist[1])
    elif num == 3:
        return "{0}_{1}_{2}".format(namelist[0], namelist[1], namelist[2])

def makePasswords(ids, numbers):
    numbers = [sha512(encode(k)).hexdigest() for k in numbers]
    return {ids[k]:([int(numbers[k][i:i+2], 16) for i in range(0, 32, 2)],
                    [int(numbers[k][i:i+2], 16) for i in range(32, 64, 2)])
            for k in range(len(numbers))}
            
def invers(word):
    return "".join(reversed(list(word)))

def AESdecrypt(vektor, password, moo=None):
    if moo == None:
        moo = AESModeOfOperation()
    return moo.decrypt(vektor, None, 2, password[0],
                       moo.aes.keySize["SIZE_128"],
                       password[1]).replace("\x00", "")

def AESencrypt(text, password, moo=None):
    if moo == None:
        moo = AESModeOfOperation()
    bla1, bla2, ciph = moo.encrypt(text, moo.modeOfOperation["CBC"],
                                   password[0], moo.aes.keySize["SIZE_128"],
                                   password[1])
    return ciph

def newConnection(server, timeout=0.025):
    read, write, important = select(server, 
                                    [], [], 
                                    timeout)
    return read

def newAuthentication(newClients, timeout=0.025):
    if newClients == []:
        return []
    read, write, important = select(newClients, 
                                    [], [], 
                                    timeout)
    return read

def newMessage(Clients, timeout=0.005):
    if Clients == []:
        return []
    read = []
    try:
        for client in Clients:
            try:
                r, w, e = select([client.sock()], [], [], timeout)
            except TypeError:
                r = []
            except ValueError:
                r = []
            if r:
                read += [client]
        return read    
    except OSError:
        return read
    
def checkNum(number):
    if not isinstance(number, int):
        return False
    if number <= 0:
        return False
    return True

def split(message):
    if not (message.count(">") >= 2 and message.count("<") >= 2):
        return None, None
    if not (message[0] == "<" and message[-1] == ">"):
        return None, None
    message = message[1:-1]
    name = message[:message.find(">")]
    text = message[message.find("<")+1:]
    if not len(name) == 30:
        return None, None
    return name, text

def getTimeString(utc):
    return "{0:0>2}.{1:0>2}.{2:0>4} {3:0>2}:{4:0>2}".format(utc.day, 
                                                            utc.month, 
                                                            utc.year, 
                                                            utc.hour, 
                                                            utc.minute)
    
def Verify(v):
    hisTime = sum([int(v[:2])*24*3600, int(v[3:5])*30*24*3600, int(v[6:10])*365*24*3600, int(v[11:13])*3600, int(v[14:16])*60])
    v2 = getTimeString(datetime.utcnow())
    myTime = sum([int(v2[:2])*24*3600, int(v2[3:5])*30*24*3600, int(v2[6:10])*365*24*3600, int(v2[11:13])*3600, int(v2[14:16])*60])
    
    if not (hisTime-myTime < 120 or myTime-hisTime < 120):
        return False
    return True

def searchDest(clients, name):
    for client in clients:
        if client.name() == name:
            return client

def run(registNums, userNames, Clients):
    
    IP = "localhost"
    port = 55559
    
    newClients = []
    
    try:
        server = socket(AF_INET, SOCK_STREAM)
        server.bind((IP, port))
        server.listen(5)
        
        
        
        knownAndConnected = []
        allIDs = [k for k in registNums]
        knownIDs = [k for k in Clients]
        allKnown = list(Clients.values())
        allRegistNums = list(registNums.values())
        allUsernames = list(userNames.values())
        allPasswords = makePasswords(allIDs, allRegistNums)
        
        running = True
        
        while running:
            read = newConnection([server])
            
            for sock in read:
                client, info = server.accept()
                newClients.append(client)
                
            #########

            read = newAuthentication(newClients)
            
            for connection in read:
                try:
                    typ = connection.recv(1024)
                    if not (typ in [b'8', b'9']):
                        raise ConnectionResetError
                    connection.send(b'1')
                    id = loads(connection.recv(1024))
                    if not (id and len(id) == 4 and id in allIDs):
                        raise ConnectionResetError
                    connection.send(b"1")
                    password = allPasswords[id]
                    
                    verification = AESdecrypt(loads(connection.recv(1024)), password)
                    if not (Verify(verification)):
                        raise ConnectionResetError
                    connection.send(b"1")
                    
                    if typ == b'8':
                        if id in knownIDs:
                            raise ConnectionResetError
                        username = AESdecrypt(loads(connection.recv(1024)), password)
                        if not (len(username) <= 30 and username != '0'*len(username)):
                            raise ConnectionResetError
                        if username in allUsernames:
                            connection.send(b"2")
                            raise ConnectionAbortedError
                        else:
                            connection.send(b'1')
                            language = AESdecrypt(loads(connection.recv(1024)), password)
                            if not language in ["fr", "de", "en"]:
                                raise ConnectionResetError
                            connection.send(b'1')
                            allUsernames.append(username)
                            userNames.update({id:username})
                            client = Client(connection, id, password, language, username)
                            allKnown.append(client)
                            Clients.update({id:client})
                            knownAndConnected.append(client)
                            
                    elif typ == b'9':
                        Clients[id].connect(connection)
                        newClients.remove(connection)
                        knownAndConnected.append(Clients[id])
                        print("looged in")
                    
                except ConnectionResetError:
                    connection.close()
                    newClients.remove(connection)
                except ConnectionAbortedError:
                    connection.close()
                    newClients.remove(connection)
            
            #########
            
            read = newMessage(knownAndConnected)
            
            for client in read:
                try:
                    print("Begin: clientMessage")
                    cc = client.recvfrom(1024)
                    print("CC of clientMessage: ", cc)
                    if not cc in ["0", "1"]:
                        print("ValueError: clientMessage.1")
                        raise ValueError
                    client.sendOK()
                    if cc == "0":
                        print("Begin if cc==0: clientMessage")
                        messageLength = client.recvfrom(1024)
                        print("clientMessage: MessageLength: ", messageLength)
                        if not (messageLength and not messageLength in [0, -1, 1]):
                            raise ConnectionResetError
                        messageLength = int(messageLength)
                        if not checkNum(messageLength):
                            raise ValueError
                        client.sendOK()
                        message = client.recvfrom(messageLength*4)
                        print("clientMessage: Message: ", message)
                        if message in [0, -1] or not len(message) == messageLength:
                            raise ValueError
                        destinataire, text = split(message)
                        print("clientMessage: Destinataire: ", destinataire)
                        print("clientMessage: Text: ", text)
                        if destinataire == None:
                            raise ConnectionResetError
                        if not (destinataire in allUsernames):
                            client.Socket.send(b'2')
                            raise KeyError
                        client.sendOK()
                        destClient = searchDest(allKnown, destinataire)
                        print("clientMessage: destinataire == destClient: ", destinataire == destClient.name())
                        destClient.sendMessage(client.name(), 
                                               "{0}><{1}".format(getTimeString(datetime.utcnow()),
                                                                   text))
                        saveMessage2(client.name().replace("0",""), destinataire.replace("0",""),
                                     getTimeString(datetime.utcnow()), text)
                        print("clientMessage: END")
                    else:
                        print("clientMessage: Begin CC = '1'")
                        messageLength = client.recvfrom(1024)
                        print("clientMessage: FirstLength: ", messageLength)
                        if not (messageLength and not messageLength in [0, -1, 1]):
                            raise ConnectionResetError
                        messageLength = int(messageLength)
                        if not checkNum(messageLength):
                            raise ValueError
                        client.sendOK()
                        message = client.recvfrom(messageLength*4)
                        print("clientMessage: FirstMessage: ", message)
                        if message in [0, -1] or not len(message) == messageLength:
                            raise ValueError
                        destinataire1, text = split(message)
                        print("clientMessage: FirstDest: ", destinataire1)
                        print("clientMessage: FirstText: ", text)
                        if destinataire1 == None:
                            raise ConnectionResetError
                        if not (destinataire1 in allUsernames):
                            client.Socket.send(b'2')
                            raise KeyError
                        client.sendOK()
                        messageLength = client.recvfrom(1024)
                        print("clientMessage: SecondLength: ", messageLength)
                        if not (messageLength and not messageLength in [0, -1, 1]):
                            raise ConnectionResetError
                        messageLength = int(messageLength)
                        if not checkNum(messageLength):
                            raise ValueError
                        client.sendOK()
                        message = client.recvfrom(messageLength*4)
                        print("clientMessage: SecondMessage: ", message)
                        if message in [0, -1] or not len(message) == messageLength:
                            raise ValueError
                        destinataire2, text = split(message)
                        print("clientMessage: SecondDest: ", destinataire2)
                        print("clientMessage: SecondText: ", text)
                        if destinataire2 == None:
                            raise ConnectionResetError
                        if not (destinataire2 in allUsernames):
                            client.Socket.send(b'2')
                            raise KeyError
                        client.sendOK()
                        print("clientMessage: Begin sendMessage to both")
                        destClient1 = searchDest(allKnown, destinataire1)
                        destClient1.sendMessage("{0} to {1}, {2}".format(client.name(), destinataire1, destinataire2), 
                                               "{0}><{1}".format(getTimeString(datetime.utcnow()),
                                                                   text))
                        destClient2 = searchDest(allKnown, destinataire2)
                        destClient2.sendMessage("{0} to {1}, {2}".format(client.name(), destinataire1, destinataire2),
                                               "{0}><{1}".format(getTimeString(datetime.utcnow()),
                                                                   text))
                        saveMessage3(client.name().replace("0",""), destinataire1.replace("0",""),
                                     destinataire2.replace("0",""), getTimeString(datetime.utcnow()), text)
                        print("clientMessage: END")
                        
                except ConnectionResetError:
                    client.disconnect()
                    knownAndConnected.remove(client)
                except ConnectionAbortedError:
                    client.disconnect()
                    knownAndConnected.remove(client)
                except ValueError:
                    client.disconnect()
                    knownAndConnected.remove(client)
                except KeyError:
                    pass
        
    finally:
        for sock in newClients:
            sock.close()
        for client in allKnown:
            client.disconnect()
            
        server.close()
            
        data1 = open("Usernames.p", "wb")
        dump(userNames, data1)
        data1.close()
        
        data2 = open("Clients.p", "wb")
        dump(Clients, data2)
        data2.close()

#################################################

try:
    data1 = open("RegistNums.p", "rb")
    registnums = load(data1)
    data1.close()
    
    try:
        data2 = open("Usernames.p", "rb")
        usernames = load(data2)
        data2.close()

        data3 = open("Clients.p", "rb")
        clients = load(data3)
        data3.close()
        
        del data2, data3
        
        run(registnums, usernames, clients)
        
    except FileNotFoundError:
        del data1
        
        run(registnums, {}, {})
    
except FileNotFoundError as e:
    pass
