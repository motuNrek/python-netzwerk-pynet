from socket import socket, AF_INET, SOCK_STREAM
from codecs import encode, decode
from select import select
from _thread import start_new_thread
from tkinter import *
from tkinter import ttk, messagebox
from time import time
from datetime import datetime
from hashlib import sha512
from aes_python3 import *
from pickle import dump, dumps, load, loads

################################################

class tk(Tk):
	def __init__(self):
		Tk.__init__(self)
		Grid.columnconfigure(self, 0, weight=1)
		Grid.rowconfigure(self, 0, weight=1)
		
		self.bind("<Control-f>", self.__fullscreen)
		self.bind("<Escape>", self.__normalscreen)
	
	def __fullscreen(self, event):
		self.overrideredirect(True)
		self.geometry("{0}x{1}+0+0".format(self.winfo_screenwidth(), self.winfo_screenheight()))
		
	def __normalscreen(self, event):
		self.overrideredirect(False)
		self.geometry("800x500+150+100")
		
class toplevel(Toplevel):
	def __init__(self, master):
		Toplevel.__init__(self, master)
		self.geometry("700x400+500+150")
		
		self.resizable(height=False, width=False)
		
class MaxEntry(Entry):
	def __init__(self, master, maxlength, **kw):
		Entry.__init__(self, master, **kw)
		
		self.variable = StringVar()
		self.variable.trace("w", lambda name, index, mode : self.__checkLength(self.variable))
		self.configure(textvariable=self.variable)
		
		self.Max = maxlength
		
	def __checkLength(self, variable):
		if len(self.get()) > self.Max:
			variable.set(self.get()[:self.Max])
		
class MessageWindow(toplevel):
    def __init__(self, master, language):
        toplevel.__init__(self, master)
        
        self.Language = language
        self.LanguageStyle = {"Senden": {"fr": "Envoyer",
                                         "de": "Senden",
                                         "en": "Send"
                                        },
                              "Destination": {"fr": "Destination: ",
                                              "de": "An: ",
                                              "en": "Destination: "
                                             },
                              "Clear": {"fr": "Effacer",
                                        "de": "Löschen",
                                        "en": "Clear"
                                       }
                              }
        self.mainframe = ttk.Frame(self)
        self.mainframe.grid(column=0, row=0)
        
        self.DestinationExpl = Label(self.mainframe, text=self.LanguageStyle["Destination"][self.Language])
        self.DestinationExpl.grid(column=0, row=0, sticky=E)
        self.Destination = MaxEntry(self.mainframe, maxlength=30,width=40)
        self.Destination.grid(column=1, row=0, columnspan=2, sticky=W)
        self.CCExpl = Label(self.mainframe, text="CC: ")
        self.CCExpl.grid(column=0, row=1, sticky=E)
        self.CC = MaxEntry(self.mainframe, maxlength=30, width=40)
        self.CC.grid(column=1, row=1, columnspan=2, sticky=W)
        
        self.MailText = Text(self.mainframe, width=87, height=22)
        self.MailText.grid(column=0, row=2, columnspan=3)
        
        self.Send = Button(self.mainframe, text=self.LanguageStyle["Senden"][self.Language], width=15, command=self.__send)
        self.Send.grid(column=0, row=3)
        
        self.Clear = Button(self.mainframe, text=self.LanguageStyle["Clear"][self.Language], width=15, command=self.__clear)
        self.Clear.grid(column=2, row=3)
        
        self.focus_set()
        self.Destination.focus_set()
        
        self.Name = False
        self.Message = False
    
    def __send(self):
        print("MessageWindow.__send: Begin")
        self.CCname = self.CC.get()
        self.Name = self.Destination.get()
        self.Message = self.MailText.get("0.0", END)[:-1]
        print("MessageWindow.__send: cc, name, message: ", 
              self.CCname, self.Name,self.Message)
        print("MessageWindow.__send: End")
        self.destroy()
        
    def __clear(self):
        self.MailText.delete(0.0, END)
        self.MailText.focus_set()

class FirstScreen(object):
    def __init__(self, master):
        self.master = master
        self.master.geometry("800x500+150+100")
        
        
        self.mainframe = ttk.Frame(master)# padding(12,0,12,12))
        self.mainframe.grid(column=0, row=0, sticky=(N,W,E,S))
        
        self.LoginUsernameLabel = ttk.Label(self.mainframe, text="Username")
        self.LoginUsernameLabel.grid(column=1, row=0, sticky=S)
        self.LoginPasswordLabel = ttk.Label(self.mainframe, text="Password")
        self.LoginPasswordLabel.grid(column=2, row=0, sticky=S, columnspan=3)

        self.Title = ttk.Label(self.mainframe, text="CryptNet", width=20)
        self.Title.grid(column=0, row=1,sticky=E)
        self.LoginUsernameEntry = MaxEntry(self.mainframe, 30, width=40)
        self.LoginUsernameEntry.grid(column=1, row=1, sticky=N)
        self.LoginPasswordEntry = MaxEntry(self.mainframe, 30, width=40, show="*")
        self.LoginPasswordEntry.grid(column=2, row=1, sticky=N, columnspan=3)

        self.RegistrationTitle = ttk.Label(self.mainframe, text='Registration')
        self.RegistrationTitle.grid(column=1, row=2, columnspan=2)
            
        self.RegistrationNumberLabel = ttk.Label(self.mainframe, text="Registration-Number")
        self.RegistrationNumberLabel.grid(column=1, row=3, sticky=E)
        self.RegistrationNumberEntry = MaxEntry(self.mainframe, 30, width=40)
        self.RegistrationNumberEntry.grid(column=2, row=3, sticky=W, columnspan=3)
        self.RegistrationNumberLabelRetype = ttk.Label(self.mainframe, text="Registration-Number (retype)")
        self.RegistrationNumberLabelRetype.grid(column=1, row=4, sticky=E)
        self.RegistrationNumberEntryRetype = MaxEntry(self.mainframe, 30, width=40)
        self.RegistrationNumberEntryRetype.grid(column=2, row=4, sticky=W, columnspan=3)
        self.RegistrationUsernameLabel = ttk.Label(self.mainframe, text="Username")
        self.RegistrationUsernameLabel.grid(column=1, row=5, sticky=E)
        self.RegistrationUsernameEntry = MaxEntry(self.mainframe, 30, width=40)
        self.RegistrationUsernameEntry.grid(column=2, row=5, sticky=W, columnspan=3)
        self.RegistrationPasswordLabel = ttk.Label(self.mainframe, text="Password")
        self.RegistrationPasswordLabel.grid(column=1, row=6, sticky=E)
        self.RegistrationPasswordEntry = MaxEntry(self.mainframe, 30, width=40, show="*")
        self.RegistrationPasswordEntry.grid(column=2, row=6, sticky=W, columnspan=3)
        self.RegistrationPasswordRetypeLabel = ttk.Label(self.mainframe, text="Password (retype)")
        self.RegistrationPasswordRetypeLabel.grid(column=1, row=7, sticky=E)
        self.RegistrationPasswordRetypeEntry = MaxEntry(self.mainframe, 30, width=40, show="*")
        self.RegistrationPasswordRetypeEntry.grid(column=2, row=7, sticky=W, columnspan=3)
        self.RegistrationLanguageLabel = ttk.Label(self.mainframe, text="Language")
        self.RegistrationLanguageLabel.grid(column=1, row=8, sticky=E, rowspan=2)
        self.Language = StringVar()
        self.RegistrationLanguageCheckFr = Checkbutton(self.mainframe, variable=self.Language, onvalue="fr")
        self.RegistrationLanguageCheckFr.grid(column=2, row=8, sticky=S)
        self.RegistrationLanguageCheckFr.select()
        Label(self.mainframe, text="fr").grid(column=2, row=9, sticky=N)
        self.RegistrationLanguageCheckEn = Checkbutton(self.mainframe, variable=self.Language, onvalue="en")
        self.RegistrationLanguageCheckEn.grid(column=3, row=8, sticky=S)
        Label(self.mainframe, text="en").grid(column=3, row=9, sticky=N)
        self.RegistrationLanguageCheckDe = Checkbutton(self.mainframe, variable=self.Language, onvalue="de")
        self.RegistrationLanguageCheckDe.grid(column=4, row=8, sticky=S)
        Label(self.mainframe, text="de").grid(column=4, row=9, sticky=N)
            
            
        self.LoginButton = ttk.Button(self.mainframe, text="Login", width=15, command=self.__login)
        self.LoginButton.grid(column=5, row=1, sticky=N)
        self.RegistrationButton = ttk.Button(self.mainframe, text="Registration", width=15, command=self.__registration)
        self.RegistrationButton.grid(column=5, row=7, rowspan=2, sticky=E)
            
        for child in self.mainframe.winfo_children(): child.grid_configure(padx=2, pady=2)
        Grid.columnconfigure(self.mainframe, 0, weight=1)
        for y in range(12): Grid.rowconfigure(self.mainframe, y, weight=1)
            
        self.RegistrationTitle["font"] = ("Arial", 15, "normal")
        self.Title["font"] = ("Arial", 20, "bold")
        
    def __login(self):
        username = self.LoginUsernameEntry.get()
        password = self.LoginPasswordEntry.get()
        if username == "" or password == "":
            return
        client = Client("login", username, password, "login")
        if client.Status == True:
            self.mainframe.destroy()
            screen = SecondScreen(self.master, username, client)
        else:
            self.LoginUsernameEntry.delete(0, END)
            self.LoginPasswordEntry.delete(0, END)
            messagebox.showerror("Alert", client.Status)
            client.close()
            del client
    
    def __registration(self):
        number1 = self.RegistrationNumberEntry.get()
        number2 = self.RegistrationNumberEntryRetype.get()
        username = self.RegistrationUsernameEntry.get()
        password1 = self.RegistrationPasswordEntry.get()
        password2 = self.RegistrationPasswordRetypeEntry.get()
        language = self.Language.get()
        
        if not (username and password1 and password2 and number1 and number2):
            return
        if not (password1 == password2):
            self.RegistrationPasswordEntry.delete(0, END)
            self.RegistrationPasswordRetypeEntry.delete(0,END)
            messagebox.showerror("Alert", "Passwords are not equal!!!")
            return
        if not (number1 == number2):
            self.RegistrationNumberEntry.delete(0,END)
            self.RegistrationNumberEntryRetype.delete(0,END)
            messagebox.showerror("Alert", "Registration-Numbers are not equal!!!")
            return
        
        client = Client(number1, username, password1, language)
        
        if client.Status == True:
            self.mainframe.destroy()
            screen = SecondScreen(self.master, username, client)
        else:
            self.RegistrationNumberEntry.delete(0, END)
            self.RegistrationNumberEntryRetype.delete(0,END)
            self.RegistrationUsernameEntry.delete(0,END)
            self.RegistrationPasswordEntry.delete(0,END)
            self.RegistrationPasswordRetypeEntry.delete(0,END)
            messagebox.showerror("Alert", client.Status)
            del client
    
    def mainloop(self):
        self.master.mainloop()

class SecondScreen(object):
    def __init__(self, master, username, client):
        self.master = master
        self.master.resizable(width=False, height=False)
            
        self.Client = client
            
        self.NewMessageLanguageStyle = {"fr": "Nouveau message", "de": "Neue Nachricht", "en": "New Message"}
            
        self.mainframe = ttk.Frame(master)
        self.mainframe.grid(column=0,row=0, sticky=(N,W,E,S))

        self.ShowMessages = Text(self.mainframe, state=DISABLED)
        self.ShowMessages.grid(column=0, row=0, rowspan=5)
            
        self.Logout = ttk.Button(self.mainframe, text="Logout", width=20, command=self.logout)
        self.Logout.grid(column=1, row=0, sticky=(W,E))
            
        self.WelcomeLabel = ttk.Label(self.mainframe, text="Hi {0}".format(username))
        self.WelcomeLabel.grid(column=1, row=2)
            
        self.NewMessage = ttk.Button(self.mainframe, text=self.NewMessageLanguageStyle[self.Client.Language], width=20, command=self.__newMessage)
        self.NewMessage.grid(column=1, row=4, sticky=(W,E))
            
        self.WaitingMessage = False
        
        client.knowMasterScreen(self)
            
        start_new_thread(self.WaitOnInteraction, ())
            
    def logout(self):
        try:
            self.Client.close()
            del self.Client
            self.mainframe.destroy()
            screen = FirstScreen(self.master)
            del self
        except AttributeError:
            pass
    
    def __newMessage(self):
        self.MailWindow = MessageWindow(self.master, self.Client.Language)
        self.WaitingMessage = True
    
    def WaitOnInteraction(self):
        print("SecondScreen.WaitOnInteraction: Begin")
        self.Running = True
        while self.Running:
            readable, w, e = select([self.Client.Socket], [], [], 0.05)
            if readable:
                self.Readable = readable
                self.__getMessage()
                self.Running = False
            else: 
                if self.WaitingMessage:
                    if self.MailWindow.Name and self.MailWindow.Message:
                        print("WaitOnInteraction: SendButtonPressed")
                        print("WaitOnInteraction: CC =", self.MailWindow.CCname)
                        if self.MailWindow.CCname:
                            print("WaitOnInteraction: CC == True")
                            print("WaitOnInteraction: Begin __sendMessage")
                            self.__sendMessage(self.MailWindow.Name,
                                               self.MailWindow.Message,
                                               self.MailWindow.CCname)
                        else:
                            print("WaitOnInteraction: CC == False")
                            print("WaitOnInteraction: Begin __sendMessage")
                            self.__sendMessage(self.MailWindow.Name,
                                               self.MailWindow.Message,
                                               None)
                        del self.MailWindow
                        self.WaitingMessage = False
                        self.Running = False
        return 0
    
    def __getMessage(self):
        try:
            print("__getMessage: Begin")
            typ = self.Client.recvfrom(64)
            print("__getMessage: typ = ", typ)
            if not typ in ["1", "2"]:
                print("typ not in ['1', '2']")
                return ValueError
            self.Client.send(b'1')
            message = self.Client.recvfrom(1024)
            print("__getMessage: Length: ", message)
            if not message:
                messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                self.logout()
                return
            length = int(message)
            self.Client.send(b'1')
            if typ == "1":
                print("__getMessage: Begin typ == '1'")
                message = self.Client.recvfrom(4*length)
                print("__getMessage: Message: ", message)
                print("__getMessage: Begin myFormat")
                message = myFormat(message)
                print("__getMessage: FormatedText: ", message)
                message = message[:32].replace("0", "")+message[32:]
                print("__getMessage: FinalText: ", message)
                self.Client.send(b'1')
            else:
                print("__getMessage: Begin typ == '2'")
                message = self.Client.recvfrom(4*length)
                print("__getMessage: Message: ", message)
                print("__getMessage: Begin myFormat2")
                message = myFormat2(message)
                print("__getMessage: FormatedText: ", message)
                message = message[:98].replace("0", "")+message[98:]
                print("__getMessage: FinalText: ", message)
                self.Client.send(b'1')
            
            self.ShowMessages.configure(state="normal")
            self.ShowMessages.insert(END, message + "\n")
            self.ShowMessages.configure(state="disabled")
            self.ShowMessages.see(END)
            
            start_new_thread(self.WaitOnInteraction, ())
                        
        except OSError:
            pass
        except ValueError:
            print("ServError: __getMessage")
            messagebox.showerror("ServerError", "ServerError")
            self.logout()
    
    def __sendMessage(self, destination, message, cc=None):
        if cc == None:
            print("__sendMessage: cc == None")
            self.Client.sendto("0")
            text = "<{0}><{1}>".format(destination.rjust(30, "0"), message)
            length = str(len(text))
            mytext = "<You to {0} {1}>\n{2:>15}\n\n".format(destination, 
                                                            getTimeString(datetime.now()), 
                                                            "<"+message+">")
            print("__sendMessage: Text: ", text)
            print("__sendMessage: Length: ", length)
            message = self.Client.recv(8)
            if message == b'1':
                pass
            elif message == b'':
                messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                self.logout()
            else:
                print("__sendMessage: ServerError1: Message wrong: ", message)
                messagebox.showerror("Alert", "ServerError")
                self.logout()
            try:
                self.Client.sendto(length)
                response = self.Client.recv(1024)
                if response == b'1':
                    print("__sendMessage: length recieved")
                    self.Client.sendto(text)
                    message = self.Client.recv(2048)
                    if message == b'1':
                        print("__sendMessage: Text recieved")
                        self.ShowMessages.configure(state="normal")
                        self.ShowMessages.insert(END, mytext)
                        self.ShowMessages.configure(state="disabled")
                        self.ShowMessages.see(END)
                        
                        start_new_thread(self.WaitOnInteraction, ())
                        print("__sendMessage: End")
                        return
                    elif message == b'2':
                        print("__sendMessage: Username unknown")
                        self.ShowMessages.configure(state="normal")
                        self.ShowMessages.insert(END, "<server {0}><ERROR: '{1}' does not exist.\n>".format(getTimeString(utc2local(datetime.utcnow())), destination))
                        self.ShowMessages.configure(state="disabled")
                        self.ShowMessages.see(END)
                        
                        start_new_thread(self.WaitOnInteraction, ())
                        print("__sendMessage: End")
                        return
                    elif message == b'':
                        messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                        self.logout()
                    else:
                        print("__sendMessage: ServerError3: message wrong: ", message)
                        messagebox.showerror("ServerError", "ServerError")
                        self.logout()
                elif response == b'':
                    messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                    self.logout()
                else:
                    print("__sendMessage: ServerError3: message wrong: ", response)
                    messagebox.showerror("ServerError", "ServerError")
                    self.logout()
            except:
                print("__sendMessage: ServerError4")
                messagebox.showerror("ServerError", "ServerError")
                self.logout()
        else:
            print("__sendMessage: cc ==", cc)
            self.Client.sendto("1")
            text1 = "<{0}><{1}>".format(destination.rjust(30, "0"), message)
            length1 = str(len(text1))
            text2 = "<{0}><{1}>".format(cc.rjust(30, "0"), message)
            length2 = str(len(text2))
            mytext = "<You to {0} {1}>\n{2:>15}\n\n".format("{0}, {1}".format(destination, cc),
                                                            getTimeString(datetime.now()), 
                                                            "<"+message+">")
            print("__sendMessage: Text1: ", text1)
            print("__sendMessage: Length1: ", length1)
            print("__sendMessage: Text2: ", text2)
            print("__sendMessage: Length2: ", length2)
            message = self.Client.recv(8)
            if message == b'1':
                pass
            elif message == b'':
                messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                self.logout()
            else:
                print("__sendMessage: ServerError1: Message wrong: ", message)
                messagebox.showerror("Alert", "ServerError")
                self.logout()
            try:
                self.Client.sendto(length1)
                response = self.Client.recv(8)
                if response == b'1':
                    print("__sendMessage: Length1 recieved")
                    self.Client.sendto(text1)
                    message = self.Client.recv(8)
                    if message == b'1':
                        print("__sendMessage: Text1 recieved")
                        self.Client.sendto(length2)
                        response = self.Client.recv(8)
                        if response == b'1':
                            print("__sendMessage: Length2 recieved")
                            self.Client.sendto(text2)
                            message = self.Client.recv(8)
                            if message == b'1':
                                print("__sendMessage: Text recieved")
                                self.ShowMessages.configure(state="normal")
                                self.ShowMessages.insert(END, mytext)
                                self.ShowMessages.configure(state="disabled")
                                self.ShowMessages.see(END)
                                
                                start_new_thread(self.WaitOnInteraction, ())
                                print("__sendMessage: END")
                                return
                            elif message == b'2':
                                print("__sendMessage: Username2 unknown")
                                self.ShowMessages.configure(state="normal")
                                self.ShowMessages.insert(END, "<server {0}><ERROR: '{1}' does not exist.\n>".format(getTimeString(utc2local(datetime.utcnow())), cc))
                                self.ShowMessages.configure(state="disabled")
                                self.ShowMessages.see(END)
                                
                                start_new_thread(self.WaitOnInteraction, ())
                                print("__sendMessage: END")
                                return
                            elif message == b'':
                                messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                                self.logout()
                            else:
                                print("__sendMessage: ServerError2: Message wrong: ", message)
                                messagebox.showerror("ServerError", "ServerError")
                                self.logout()
                                
                        elif response == b'':
                            messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                            self.logout()
                        else:
                            print("__sendMessage: ServerError3: Message wrong: ", response)
                            messagebox.showerror("ServerError", "ServerError")
                            self.logout()
                            
                    elif message == b'2':
                        print("__sendMessage: Username1 unknown")
                        self.ShowMessages.configure(state="normal")
                        self.ShowMessages.insert(END, "<server {0}><ERROR: '{1}' does not exist.\n>".format(getTimeString(utc2local(datetime.utcnow())), destination))
                        self.ShowMessages.configure(state="disabled")
                        self.ShowMessages.see(END)
                        
                        start_new_thread(self.WaitOnInteraction, ())
                        print("__sendMessage: END")
                        return
                    elif message == b'':
                        messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                        self.logout()
                    else:
                        print("__sendMessage: ServerError4: Message wrong: ", message)
                        messagebox.showerror("ServerError", "ServerError")
                        self.logout()
                elif response == b'':
                    messagebox.showerror("Alert", self.Client.ConnLost[self.Client.Language])
                    self.logout()
                else:
                    print("__sendMessage: ServerError5: Message wrong: ", response)
                    messagebox.showerror("ServerError", "ServerError")
                    self.logout()
            except:
                print("__sendMessage: ServerError6")
                messagebox.showerror("ServerError", "ServerError")
                self.logout()

class Client(object):
    def __init__(self, registNum, username, password, language):
        self.MOO = AESModeOfOperation()
        IP = 'localhost'
        port = 55559
        
        self.Username = username.rjust(30, "0")
        self.Name = username
        self.UserPass = password
        
        self.ConnLost = {"fr": "Connection perdue", 
                         "de": "Verbindung abgebrochen", 
                         "en": "Connection lost"}
        self.ConnFailed = {"fr": "Essaie de connexion a échoué", 
                           "de": "Verbindung fehlgeschlagen", 
                           "en": "Connection failed"}
        self.LogFailed = {"fr": "Le login a échoué", 
                          "de": "Login fehlgeschlagen", 
                          "en": "Login failed"}
        self.ServError = {"fr": "Problèmes au niveau du serveur", 
                          "de": "Probleme beim Server aufgetreten", 
                          "en": "Server Error"}
        self.RegistFailed = {"fr": "L'enregistrement a échoué", 
                             "de": "Registrierung fehlgeschlagen", 
                             "en": "Registration failed"}
        self.UsernameKnown = {"fr": "Votre nom d'utilisateur es déjà utilisé",
                             "de": "Ihr Benutzername existiert leider schon",
                             "en": "Your username ist already in use"}

        self.__getFilename()
        self.__getKeys("user")
        
        self.State = 0
        self.Status = False
        
        self.Socket = socket(AF_INET, SOCK_STREAM)
        try:
            self.Socket.connect((IP, port))
        except:
            self.Status = self.ConnFailed["en"]
            return
        
        if registNum == "login":
            self.__login()
            return
        else:
            self.RegistNum = registNum
            self.ID = self.RegistNum[:4]
            self.Language = language
            self.Identity = "{0};{1};{2};{3}".format(self.ID, self.RegistNum, 
                                                 self.Name, self.Language)
            self.__registration()
            return
            
    def knowMasterScreen(self, master):
        self.Master = master
            
    def __getFilename(self):
        self.MyFile = "projektDaten_{0}.tpe".format(self.Name)
        return 0
    
    def __getKeys(self, id="normal"):
        if id == "user":
            hashPass = sha512(encode(self.UserPass)).hexdigest()
            self.UserPass = ([int(hashPass[i:i+2], 16) for i in range(0, 32, 2)],
                             [int(hashPass[i:i+2], 16) for i in range(32, 64, 2)])
            return
        hashNumber = sha512(encode(self.RegistNum)).hexdigest()
        self.Password = ([int(hashNumber[i:i+2], 16) for i in range(0, 32, 2)],
                         [int(hashNumber[i:i+2], 16) for i in range(32, 64, 2)])
    
    def __getIdentityFromFile(self):
        try:
            data = open(self.MyFile, "rb")
            ciph = load(data)
            data.close()
            self.Identity = AESdecrypt(ciph, self.UserPass, self.MOO)
            self.ID, self.RegistNum, name, self.Language = self.Identity.split(";")
            if not (self.ID == self.RegistNum[:4] and self.Language in ["fr", "de", "en"] and name == self.Name):
                self.Status = "False Password"
                return
            
        except FileNotFoundError:
            self.Status = "Not registred"
            return
        except ValueError:
            self.Status = "False Password"
            return
    
    def __giveIdentityToFile(self):
        ciph = AESencrypt(self.Identity, self.UserPass, self.MOO)
        data = open(self.MyFile, "wb")
        dump(ciph, data)
        data.close()
    
    def __registration(self):
        self.__getKeys()
        self.Socket.send(b'8')
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __registration.1")
                self.Status = self.ServError[self.Language]
                return
        self.Socket.send(dumps(self.ID))
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __registration.2")
                self.Status = self.ServError[self.Language]
                return
        self.Socket.send(dumps(AESencrypt(getTimeString(datetime.utcnow()),
                                         self.Password, self.MOO)))
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __registration.3")
                self.Status = self.ServError[self.Language]
                return
        self.Socket.send(dumps(AESencrypt(self.Username, self.Password,
                                          self.MOO)))
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            elif message == b'2':
                self.Status = self.UsernameKnown[self.Language]
                return
            else:
                print("ServError: __registration.4")
                self.Status = self.ServError[self.Language]
                return
        self.Socket.send(dumps(AESencrypt(self.Language, self.Password,
                                          self.MOO)))
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __registration.5")
                self.Status = self.ServError[self.Language]
                return
        self.__giveIdentityToFile()
        self.Status = True
    
    def __login(self):
        self.__getIdentityFromFile()
        if self.Status != 0:
            return
        self.__getKeys()
        self.Socket.send(b'9')
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __login.1")
                self.Status = self.ServError[self.Language]
                return
        self.Socket.send(dumps(self.ID))
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __login.2")
                self.Status = self.ServError[self.Language]
                return
        self.Socket.send(dumps(AESencrypt(getTimeString(datetime.utcnow()),
                                         self.Password, self.MOO)))
        message = self.Socket.recv(8)
        if (not message == b'1'):
            if message == b'':
                self.Status = self.ConnLost[self.Language]
                return
            else:
                print("ServError: __login.3")
                self.Status = self.ServError[self.Language]
                return
        self.Status = True
        print("logged in")
        
    def sendto(self, message):
        try:
            return self.Socket.send(dumps(AESencrypt(message, self.Password,
                                                     self.MOO)))
        except:
            print("ServError: sendto")
            messagebox.showerror("ServerError", "ServerError")
            self.Master.logout()
    
    def recvfrom(self, length):
        try:
            message = self.Socket.recv(length)
            return AESdecrypt(loads(message), self.Password,
                              self.MOO)
        except:
            print("ServError: recvfrom")
            print(message)
            messagebox.showerror("ServerError", "ServerError")
            self.Master.logout()
            
    def close(self):
        return self.Socket.close()
    
    def send(self, message):
        return self.Socket.send(message)
    
    def recv(self, length):
        message = self.Socket.recv(length)
        return message

################################################

def utc2local(utcDatetime):
    now_timestamp = time()
    offset = datetime.fromtimestamp(now_timestamp) - datetime.utcfromtimestamp(now_timestamp)
    return utcDatetime + offset

def getTimeString(utc):
    return "{0:0>2}.{1:0>2}.{2:0>4} {3:0>2}:{4:0>2}".format(utc.day, 
                                                            utc.month, 
                                                            utc.year, 
                                                            utc.hour, 
                                                            utc.minute)

def AESencrypt(text, password, moo=None):
    if moo == None:
        moo = AESModeOfOperation()
    bla1, bla2, ciph = moo.encrypt(text, moo.modeOfOperation["CBC"],
                                   password[0], moo.aes.keySize["SIZE_128"],
                                   password[1])
    return ciph

def AESdecrypt(vektor, password, moo=None):
    if moo == None:
        moo = AESModeOfOperation()
    return moo.decrypt(vektor, None, 2, password[0],
                       moo.aes.keySize["SIZE_128"],
                       password[1]).replace("\x00","")
                       
def myFormat(text):
    print("myFormat: Begin")
    myday = int(text[33:35])
    mymonth = int(text[36:38])
    myyear = int(text[39:43])
    myhour = int(text[44:46])
    myminute = int(text[47:49])
    mytime = getTimeString(utc2local(datetime(myyear, mymonth, myday, myhour, myminute, 0, 0)))
    print("myFormat: END")
    return text[:31] + " {0}".format(mytime) + text[49:]
    
def myFormat2(text):
    print("myFormat2: Begin")
    myday = int(text[99:101])
    mymonth = int(text[102:104])
    myyear = int(text[105:109])
    myhour = int(text[110:112])
    myminute = int(text[113:115])
    mytime = getTimeString(utc2local(datetime(myyear, mymonth, myday, myhour, myminute, 0, 0)))
    print("myFormat2: END")
    return text[:97] + " {0}".format(mytime) + text[115:]

################################################

FirstScreen(tk()).mainloop()